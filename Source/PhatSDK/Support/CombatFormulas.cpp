#include <StdAfx.h>
#include "PhatSDK.h"
#include "CombatFormulas.h"

double GetImbueMultiplier(double currentSkill, double minEffectivenessSkill, double maxEffectivenessSkill, double maxMultiplier, bool allowNegative)
{
	double multiplier = (currentSkill - minEffectivenessSkill) / (maxEffectivenessSkill - minEffectivenessSkill);
	double value = multiplier * maxMultiplier;
	if (!allowNegative)
	{
		value = max(value, 0.0);
	}
	value = min(value, maxMultiplier);
	return value;
}

void CalculateDamage(DamageEventData *dmgEvent, SpellCastData *spellData)
{
	assert(dmgEvent);
	assert(dmgEvent->source);
	assert(dmgEvent->target);

	dmgEvent->damageBeforeMitigation = dmgEvent->damageAfterMitigation = dmgEvent->baseDamage;

	if (dmgEvent->source->_IsPlayer() && dmgEvent->target->_IsPlayer())
		dmgEvent->isPvP = true;

	CalculateRendingAndMiscData(dmgEvent);
	CalculateAttributeDamageBonus(dmgEvent);
	CalculateSkillDamageBonus(dmgEvent, spellData);
	CalculateSlayerData(dmgEvent);
	CalculateRatingData(dmgEvent);

	double damageCalc;
	
	
	if(dmgEvent->damage_form != DF_MAGIC) {
		damageCalc = (dmgEvent->baseDamage + dmgEvent->skillDamageBonus * dmgEvent->varianceRoll) * dmgEvent->attributeDamageMod * dmgEvent->slayerDamageMod * dmgEvent->attackPower;

		//Don't let low power bar + low damage weapon result in constantly hitting for zero.
		if(damageCalc < 1.0) {
			damageCalc = 1.0;
		}

		if(dmgEvent->wasCrit && !dmgEvent->critDefended) {
			damageCalc *= dmgEvent->critMultiplier;
		}
	} else { //Magic damage computation

		if(!dmgEvent->wasCrit || dmgEvent->critDefended) {
			damageCalc = (dmgEvent->baseDamage + dmgEvent->skillDamageBonus) * dmgEvent->slayerDamageMod;
		} else {
			//critBonus = (dmgEvent->isPvP ? spell->minDmg : spell->maxDmg) * 0.5f;

			damageCalc = dmgEvent->critMultiplier * dmgEvent->baseDamage;
			damageCalc += dmgEvent->baseDamage * dmgEvent->slayerDamageMod;
			damageCalc += dmgEvent->skillDamageBonus;
		}
	}

	if (dmgEvent->damageRatingMod)
		damageCalc *= dmgEvent->damageRatingMod;

	dmgEvent->damageBeforeMitigation = dmgEvent->damageAfterMitigation = damageCalc;
}

void CalculateAttributeDamageBonus(DamageEventData *dmgEvent)
{
	assert(dmgEvent);
	assert(dmgEvent->source);
	
	switch (dmgEvent->damage_form)
	{
	case DF_MELEE:
	case DF_MISSILE:
	{
		uint32_t attrib = 0;
		if (dmgEvent->attackSkill == DAGGER_SKILL || dmgEvent->attackSkill == BOW_SKILL || dmgEvent->attackSkill == CROSSBOW_SKILL)
			dmgEvent->source->m_Qualities.InqAttribute(COORDINATION_ATTRIBUTE, attrib, FALSE);
		else
			dmgEvent->source->m_Qualities.InqAttribute(STRENGTH_ATTRIBUTE, attrib, FALSE);


		/*
			Page 245 of Sybex Strategy Guide gives this note:

			"StrMod is a modifier based on the attacker's Strength. It is a negative modifier
			for Strength lower than 55; after that, it becomes a positive modifier. For instance, at
			maximum starting Strength of 100, the modifier is around 1.36"

			I assume that when they say "negative modifier", the mean that it reduces the damage (mod < 1.0), not
			that the value itself is negative. The formula they give on page 244 is a product of a large
			number of factors, so if one were negative, then the whole product would be negative damage,
			which surely isn't the intention.

			Assuming the formula is a linear function, then we can compute the slope:
				(1) 1.0 + (100-55)*X = 1.36
				(2) 45*X = 0.36
				(3) X = 0.36 / 45
				(4) X = 0.008

			attribute mod = 1.0 + (attr - 55) * 0.008
				
		*/
		dmgEvent->attributeDamageMod = 1.0f + ((int)attrib - 55) * 0.008;

		break;
	}
	case DF_MAGIC:
		break;
	}
}

void CalculateSkillDamageBonus(DamageEventData *dmgEvent, SpellCastData *spellData)
{
	if (!dmgEvent)
		return;
	if (!dmgEvent->source)
		return;

	switch (dmgEvent->damage_form)
	{
	case DF_MELEE:
		if(dmgEvent->attackSkill == UNARMED_COMBAT_SKILL && dmgEvent->source->AsPlayer()) {
			/*
				Page 148 of Sybex Strategy Guide gives this note:

				"The base damage done by punching and kicking is calculated using the attacker's
				Unarmed Combat skill modified by the type of armor they are wearing:

				BaseDmg = 1 + ArmorDmg + (Skill/20)

				_not rounded_, where "Skill" is the attacker's Unarmed Combat skill and "ArmorDmg" is
				the amount of damage the armor (or clothing) the character is wearing adds to the equation,
				as follows [...]"

				They continue:
				"Note that if you are wielding an unarmed combat weapon such as a Katar or Nekode, this [guantlet/boot] damage bonus does not apply;
				the damage and other statistics of the weapon, plus your Unarmed Combat skill, determine the type and amount of damage you do."

				This implies that the UA skill bonus is also attached to weapons.
				
				However, this does mean that the skill bonus _is_ affected by the variance roll because it is part of the base damage before variance
				is computed. It's somewhat vague about whether UA damage bonus is applied pre- or post-variance roll when wielding a weapon, but
				a rational reading of it would be that it should be pre-variance (i.e. exactly as punching without a weapon), otherwise, wielding
				a weapon would essentially get a zero variance damage bonus.

				An old forum post (not authoritative) also mentions this formula, but of course the official
				strategy guide is a better source.
				https://forums.penny-arcade.com/discussion/35347/asherons-call-nine-years-of-killing-olthoi/p12

				Note that "BaseDmg" 
			*/
			dmgEvent->skillDamageBonus = (double)dmgEvent->attackSkillLevel / 20.0;
			break;
		}

	case DF_MISSILE:
		return;
	case DF_MAGIC:
		if (!spellData)
			return;

		if (dmgEvent->attackSkill == WAR_MAGIC_SKILL)
		{
			ProjectileSpellEx *meta = (ProjectileSpellEx *)spellData->spellEx->_meta_spell._spell;
			//Skill based damage bonus: This additional damage will be a constant percentage of the minimum damage value.
			//The percentage is determined by comparing the level of the spell against the buffed war magic skill of the character.
			//Note that creatures do not receive this bonus.
			if (dmgEvent->source->AsPlayer())
			{
				float minDamage = (float)meta->_baseIntensity;

				float difficulty = min(spellData->spell->_power, 350);

				float skillDamageMod = ((int)spellData->current_skill - difficulty) / 1000.0; //better made up formula.
				if (skillDamageMod > 0)
					dmgEvent->skillDamageBonus = minDamage * skillDamageMod;
			}
		}
		return;
	}
}

void CalculateCriticalHitData(DamageEventData *dmgEvent, SpellCastData *spellData)
{
	assert(dmgEvent);
	assert(dmgEvent->source);
	assert(dmgEvent->target);

	bool isPvP = dmgEvent->source->AsPlayer() && dmgEvent->target->AsPlayer();

	uint32_t imbueEffects;

	switch (dmgEvent->damage_form)
	{
	case DF_MELEE:
		dmgEvent->critChance = 0.1;
		dmgEvent->critMultiplier = 2.0;

		if(!dmgEvent->weapon)
			return;

		imbueEffects = dmgEvent->weapon->GetImbueEffects();

		if (dmgEvent->weapon->GetBitingStrikeFrequency())
			dmgEvent->critChance = dmgEvent->weapon->GetBitingStrikeFrequency();

		if (dmgEvent->weapon->GetCrushingBlowMultiplier())
			dmgEvent->critMultiplier = 1.0f + dmgEvent->weapon->GetCrushingBlowMultiplier();

		if (imbueEffects & CriticalStrike_ImbuedEffectType)
			dmgEvent->critChance += GetImbueMultiplier(dmgEvent->attackSkillLevel, 150, 400, 0.5);

		if (imbueEffects & CripplingBlow_ImbuedEffectType)
			dmgEvent->critMultiplier = 1.0f + GetImbueMultiplier(dmgEvent->attackSkillLevel, 150, 400, 6);

		if (bool critExpertise = dmgEvent->source->m_Qualities.GetInt(AUGMENTATION_CRITICAL_EXPERTISE_INT, 0))
			dmgEvent->critChance += 0.01;

		dmgEvent->critMultiplier = min(max(dmgEvent->critMultiplier, 0.0), 7.0);
		dmgEvent->critChance = min(max(dmgEvent->critChance, 0.0), 0.5);
		return;
	case DF_MISSILE:
		dmgEvent->critChance = 0.1;
		dmgEvent->critMultiplier = 2.0;

		if(!dmgEvent->weapon)
			return;

		imbueEffects = dmgEvent->weapon->GetImbueEffects();

		if (dmgEvent->weapon->GetBitingStrikeFrequency())
			dmgEvent->critChance = dmgEvent->weapon->GetBitingStrikeFrequency();

		if (dmgEvent->weapon->GetCrushingBlowMultiplier())
			dmgEvent->critMultiplier = 1+ dmgEvent->weapon->GetCrushingBlowMultiplier();

		if (imbueEffects & CriticalStrike_ImbuedEffectType)
			dmgEvent->critChance += GetImbueMultiplier(dmgEvent->attackSkillLevel, 125, 360, 0.5);

		if (imbueEffects & CripplingBlow_ImbuedEffectType)
			dmgEvent->critMultiplier = 1+GetImbueMultiplier(dmgEvent->attackSkillLevel, 125, 360, 6);

		if (bool critExpertise = dmgEvent->source->m_Qualities.GetInt(AUGMENTATION_CRITICAL_EXPERTISE_INT, 0))
			dmgEvent->critChance += 0.01;

		dmgEvent->critMultiplier = min(max(dmgEvent->critMultiplier, 0.0), 7.0);
		dmgEvent->critChance = min(max(dmgEvent->critChance, 0.0), 0.5);
		return;
	case DF_MAGIC:
		dmgEvent->critChance = 0.05;
		dmgEvent->critMultiplier = 1.5;

		if (!spellData || !dmgEvent->weapon)
			return;

		imbueEffects = dmgEvent->weapon->GetImbueEffects();

		if(dmgEvent->weapon->GetBitingStrikeFrequency())
		dmgEvent->critChance = dmgEvent->weapon->GetBitingStrikeFrequency();

		if(dmgEvent->weapon->GetCrushingBlowMultiplier())
		dmgEvent->critMultiplier = 1.0f + dmgEvent->weapon->GetCrushingBlowMultiplier();

		if (dmgEvent->attackSkill == WAR_MAGIC_SKILL)
		{
			//Imbue and slayer effects for War Magic now scale from minimum effectiveness at 125 to 
			//maximum effectiveness at 360 skill instead of from 150 to 400 skill(PvM only).

			if (imbueEffects & CriticalStrike_ImbuedEffectType)
			{
				//Critical Strike for War Magic scales from 5% critical hit chance to 50% critical hit chance at maximum effectiveness.
				//PvP: Critical Strike for War Magic scales from 5% critical hit chance to 25% critical hit chance at maximum effectiveness.
				if (dmgEvent->isPvP)
					dmgEvent->critChance += GetImbueMultiplier(dmgEvent->attackSkillLevel, 150, 400, 0.25);
				else
					dmgEvent->critChance += GetImbueMultiplier(dmgEvent->attackSkillLevel, 125, 360, 0.5);
			}

			if (imbueEffects & CripplingBlow_ImbuedEffectType)
			{
				//Crippling Blow for War Magic currently scales from adding 50% of the spells damage
				//on critical hits to adding 500% at maximum effectiveness.
				//PvP: Crippling Blow for War Magic currently scales from adding 50 % of the spells damage on critical hits 
				//to adding 100 % at maximum effectiveness
				if (dmgEvent->isPvP)
					dmgEvent->critMultiplier = 1.0f + GetImbueMultiplier(dmgEvent->attackSkillLevel, 150, 400, 0.5);
				else
					dmgEvent->critMultiplier = 1.0f + GetImbueMultiplier(dmgEvent->attackSkillLevel, 125, 360, 5.0);
			}
		}
		
		if (bool critExpertise = dmgEvent->source->m_Qualities.GetInt(AUGMENTATION_CRITICAL_EXPERTISE_INT, 0))
			dmgEvent->critChance += 0.01;

		if (isPvP)
		{
			dmgEvent->critChance = min(max(dmgEvent->critChance, 0.0), 0.25);
			dmgEvent->critMultiplier = min(max(dmgEvent->critMultiplier, 0.0), 2.0);
		}
		else
		{
			dmgEvent->critChance = min(max(dmgEvent->critChance, 0.0), 0.50);
			dmgEvent->critMultiplier = min(max(dmgEvent->critMultiplier, 0.0), 5.0);
		}

		return;
	}
}

void CalculateSlayerData(DamageEventData *dmgEvent)
{
	assert(dmgEvent);
	assert(dmgEvent->source);
	assert(dmgEvent->target);

	if(!dmgEvent->weapon) //e.g. spell traps
		return;

	if (dmgEvent->damage_form == DF_MAGIC && !dmgEvent->isProjectileSpell)
		return; //non projectile spells do not benefit from the slayer property.

	int slayerType = dmgEvent->weapon->InqIntQuality(SLAYER_CREATURE_TYPE_INT, 0, TRUE);
	if (slayerType && slayerType == dmgEvent->target->InqIntQuality(CREATURE_TYPE_INT, 0, TRUE)) {
		double slayerDamageMod = dmgEvent->weapon->InqFloatQuality(SLAYER_DAMAGE_BONUS_FLOAT, 0.0, FALSE);

		if(slayerDamageMod > 0.0)
			dmgEvent->slayerDamageMod = (float)slayerDamageMod;
	}
}

void CalculateRendingAndMiscData(DamageEventData *dmgEvent)
{
	assert(dmgEvent);
	assert(dmgEvent->source);

	dmgEvent->ignoreMagicResist = dmgEvent->source->InqBoolQuality(IGNORE_MAGIC_RESIST_BOOL, FALSE);
	dmgEvent->ignoreMagicArmor = dmgEvent->source->InqBoolQuality(IGNORE_MAGIC_ARMOR_BOOL, FALSE);

	if(!dmgEvent->weapon)
		return;

	if (!dmgEvent->ignoreMagicResist)
		dmgEvent->ignoreMagicResist = dmgEvent->weapon->InqBoolQuality(IGNORE_MAGIC_RESIST_BOOL, FALSE);

	if (!dmgEvent->ignoreMagicArmor)
		dmgEvent->ignoreMagicArmor =dmgEvent->weapon->InqBoolQuality(IGNORE_MAGIC_ARMOR_BOOL, FALSE);

	uint32_t imbueEffects = dmgEvent->weapon->GetImbueEffects();

	if (imbueEffects & IgnoreAllArmor_ImbuedEffectType)
		dmgEvent->ignoreArmorEntirely = true;

	if (imbueEffects & ArmorRending_ImbuedEffectType)
		dmgEvent->isArmorRending = true;

	switch (dmgEvent->damage_type)
	{
	case SLASH_DAMAGE_TYPE:
		if (imbueEffects & SlashRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	case PIERCE_DAMAGE_TYPE:
		if (imbueEffects & PierceRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	case BLUDGEON_DAMAGE_TYPE:
		if (imbueEffects & BludgeonRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	case COLD_DAMAGE_TYPE:
		if (imbueEffects & ColdRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	case FIRE_DAMAGE_TYPE:
		if (imbueEffects & FireRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	case ACID_DAMAGE_TYPE:
		if (imbueEffects & AcidRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	case ELECTRIC_DAMAGE_TYPE:
		if (imbueEffects & ElectricRending_ImbuedEffectType)
			dmgEvent->isElementalRending = true;
		break;
	}

	if (dmgEvent->isElementalRending)
	{
		switch (dmgEvent->damage_form)
		{
		case DF_MELEE:
			dmgEvent->rendingMultiplier = max(GetImbueMultiplier(dmgEvent->attackSkillLevel, 0, 400, 2.5), 1.0);
			break;
		case DF_MISSILE:
		case DF_MAGIC:
			dmgEvent->rendingMultiplier = max(0.25 + GetImbueMultiplier(dmgEvent->attackSkillLevel, 0, 360, 2.25), 1.0);
			break;
		default:
			return;
		}
	}

	int resistanceMod;

	if (dmgEvent->weapon->m_Qualities.InqInt(RESISTANCE_MODIFIER_TYPE_INT, resistanceMod))
		dmgEvent->isResistanceCleaving = TRUE;

	if (dmgEvent->isResistanceCleaving)
	{
		if (resistanceMod == dmgEvent->damage_type)
			dmgEvent->isElementalRending = true;

		switch (dmgEvent->damage_form)
		{
		case DF_MELEE:
			dmgEvent->rendingMultiplier = 2.5;
			break;
		case DF_MISSILE:
		case DF_MAGIC:
			dmgEvent->rendingMultiplier = 2.25;
			break;
		default:
			return;
		}
	}

	if (dmgEvent->isArmorRending)
	{
		switch (dmgEvent->damage_form)
		{
		case DF_MELEE:
			dmgEvent->armorRendingMultiplier = 1.0 / max(GetImbueMultiplier(dmgEvent->attackSkillLevel, 0, 400, 2.5), 1.0);
			break;
		case DF_MISSILE:
			dmgEvent->armorRendingMultiplier = 1.0 / max(0.25 + GetImbueMultiplier(dmgEvent->attackSkillLevel, 0, 360, 2.25), 1.0);
			break;
		case DF_MAGIC:
		default:
			return;
		}
	}

	if (dmgEvent->weapon->InqFloatQuality(IGNORE_ARMOR_FLOAT, 0, FALSE))
		dmgEvent->isArmorCleaving = TRUE;

	if (dmgEvent->isArmorCleaving)
	{
		switch (dmgEvent->damage_form)
		{
		case DF_MELEE:
			dmgEvent->armorRendingMultiplier = 1.0 / 2.5;
			break;

		case DF_MISSILE:
			dmgEvent->armorRendingMultiplier = 1.0 / 2.25;
			break;
		case DF_MAGIC:
		default:
			return;
		}
	}

}

void CalculateRatingData(DamageEventData *dmgEvent)
{
	assert(dmgEvent);
	assert(dmgEvent->source);
	assert(dmgEvent->target);

	Skill skill;
	float ratingMod = 1.0;

	// Damage
	ratingMod += dmgEvent->source->m_Qualities.GetAddRating(DAMAGE_RATING_INT, false);
	// Damage Resistance - Defender
	ratingMod -= dmgEvent->target->m_Qualities.GetAddRating(DAMAGE_RESIST_RATING_INT, false);
	// Weakness
	ratingMod -= dmgEvent->source->m_Qualities.GetAddRating(WEAKNESS_RATING_INT, false);
	// Damage Reduction
	ratingMod -= dmgEvent->source->m_Qualities.GetAddRating(AUGMENTATION_DAMAGE_REDUCTION_INT, false);
		// Recklessness - Defender
	ratingMod += dmgEvent->target->m_Qualities.GetAddRating(RECKLESSNESS_RATING_INT, false);
	// Recklessness
	if (dmgEvent->isReckless)
		ratingMod += dmgEvent->source->m_Qualities.GetAddRating(RECKLESSNESS_RATING_INT, false);
	// Sneak Attack
	if (dmgEvent->isSneakAttack)
		ratingMod += dmgEvent->source->m_Qualities.GetAddRating(SNEAK_ATTACK_RATING_INT, false);
	// Lum Aug Damage
	ratingMod += dmgEvent->source->m_Qualities.GetAddRating(LUM_AUG_DAMAGE_RATING_INT, false);
	// Lum Aug Damage Resistance - Defender
	ratingMod -= dmgEvent->target->m_Qualities.GetAddRating(LUM_AUG_DAMAGE_REDUCTION_RATING_INT, false);

	// Criticals
	if (dmgEvent->wasCrit)
	{
		// Critical Damage
		ratingMod += dmgEvent->source->m_Qualities.GetAddRating(CRIT_DAMAGE_RATING_INT, false);
		// Critical Damage Resistance - Defender
		ratingMod -= dmgEvent->target->m_Qualities.GetAddRating(CRIT_DAMAGE_RESIST_RATING_INT, false);
		// Lum Aug Critical Damage
		ratingMod += dmgEvent->source->m_Qualities.GetAddRating(LUM_AUG_CRIT_DAMAGE_RATING_INT, false);
		// Lum Aug Critical Damage Resistance - Defender
		ratingMod -= dmgEvent->target->m_Qualities.GetAddRating(LUM_AUG_CRIT_REDUCTION_RATING_INT, false);
	}

	// PvP
	if (dmgEvent->isPvP)
	{
		// PK Damage
		ratingMod += dmgEvent->source->m_Qualities.GetAddRating(PK_DAMAGE_RATING_INT, false);
		// PK Damage Resistance - Defender
		ratingMod -= dmgEvent->target->m_Qualities.GetAddRating(PK_DAMAGE_RESIST_RATING_INT, false);
	}

	dmgEvent->damageRatingMod = ratingMod;
}

void CalculateAttackConditions(DamageEventData *dmgEvent, float attackPower, double angle)
{
	assert(dmgEvent);
	assert(dmgEvent->source);
	assert(dmgEvent->target);

	Skill skill;
	int rating = 0;
	uint32_t skillLevel = 0;
	float skillMod = 0.0f;

	if (dmgEvent->wasCrit)
	{
		if (bool critDefense = dmgEvent->target->m_Qualities.GetInt(AUGMENTATION_CRITICAL_DEFENSE_INT, 0))
		{
			if (Random::GenFloat(0.0, 1.0) < (dmgEvent->source->_IsPlayer() ? 0.05 : 0.25))
			{
				// do not apply crit multiplier - treat this as a normal hit.
				dmgEvent->critDefended = true;
				dmgEvent->attackConditions |= 1;
			}
		}

		// Recklessness does not affect critical damage.
		dmgEvent->source->m_Qualities.SetInt(RECKLESSNESS_RATING_INT, 0);
	}

	if (!dmgEvent->wasCrit)
	{
		if (dmgEvent->source->m_Qualities.InqSkill(RECKLESSNESS_SKILL, skill))
		{
			// Wiki states that Recklessness only procs between 20% and 80% power but the client seems to show 10% and 90%
			if (skill._sac >= TRAINED_SKILL_ADVANCEMENT_CLASS && attackPower > 0.1f && attackPower < 0.9f)
			{
				rating = skill._sac >= SPECIALIZED_SKILL_ADVANCEMENT_CLASS ? 20 : 10;
				dmgEvent->source->InqSkill(RECKLESSNESS_SKILL, skillLevel, false);

				if (skillLevel < dmgEvent->attackSkillLevel)
				{
					skillMod = (float)skillLevel / (float)dmgEvent->attackSkillLevel;
					rating *= skillMod;
				}

				dmgEvent->source->m_Qualities.SetInt(RECKLESSNESS_RATING_INT, rating);
				dmgEvent->isReckless = true;
				dmgEvent->attackConditions |= 2;
			}
			else
			{
				// Not Reckless so set the rating int to zero so you no longer take increased damage.
				dmgEvent->source->m_Qualities.SetInt(RECKLESSNESS_RATING_INT, 0);
			}
		}
	}

	if (dmgEvent->source->m_Qualities.InqSkill(SNEAK_ATTACK_SKILL, skill))
	{
		if (skill._sac >= TRAINED_SKILL_ADVANCEMENT_CLASS)
		{
			rating = skill._sac >= SPECIALIZED_SKILL_ADVANCEMENT_CLASS ? 20 : 10;
			dmgEvent->source->InqSkill(SNEAK_ATTACK_SKILL, skillLevel, false);

			if (skillLevel < dmgEvent->attackSkillLevel)
			{
				skillMod = (float)skillLevel / (float)dmgEvent->attackSkillLevel;
				rating *= skillMod;
			}

			// If behind the target then this is a sneak attack.
			if (angle >= 90 && angle <= 270)
			{
				dmgEvent->source->m_Qualities.SetInt(SNEAK_ATTACK_RATING_INT, rating);
				dmgEvent->isSneakAttack = true;
				dmgEvent->attackConditions |= 4;
			}
			else
			{
				dmgEvent->source->m_Qualities.InqSkill(DECEPTION_SKILL, skill);

				// Max chance is 10% for trained and 15% for Specialized Deception.
				if (skill._sac >= TRAINED_SKILL_ADVANCEMENT_CLASS)
				{
					float chance = 0.0f;
					float roll = Random::GenFloat(0.0, 1.0);

					chance = skill._sac >= SPECIALIZED_SKILL_ADVANCEMENT_CLASS ? 0.15f : 0.1f;

					dmgEvent->source->InqSkill(DECEPTION_SKILL, skillLevel, false);

					// If Deception is below 306 your chance is reduced proportionately.
					if (skillLevel < 306)
					{
						chance *= min(((float)skillLevel / 306.0f), 1.0f);
					}

					if (roll < chance)
					{
						dmgEvent->target->m_Qualities.InqSkill(PERSONAL_APPRAISAL_SKILL, skill);

						// Assess Person can reduce the additional sneak attack damage from the front by up to 100% of the sneak attack bonus.
						if (skill._sac >= TRAINED_SKILL_ADVANCEMENT_CLASS)
						{
							dmgEvent->target->InqSkill(PERSONAL_APPRAISAL_SKILL, skillLevel, false);

							// If Assess Person is 306 or above the damage rating is nullified. Otherwise it is reduced proportionately.
							if (skillLevel < 306)
							{
								rating *= min(((float)skillLevel / 306.0f), 1.0f);
							}
							else
								rating = 0;
						}

						if (rating > 0)
							dmgEvent->source->m_Qualities.SetInt(SNEAK_ATTACK_RATING_INT, rating);

						dmgEvent->isSneakAttack = true;
						dmgEvent->attackConditions |= 4;
					}
					else
					{
						// Not a Sneak Attack so set rating to 0
						dmgEvent->source->m_Qualities.SetInt(SNEAK_ATTACK_RATING_INT, 0);
					}
				}
			}
		}
	}

}